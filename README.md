# Quarkus
1. To create a quarkus application run:
    mvn io.quarkus:quarkus-maven-plugin:1.5.1.Final:create

2. Go here to find the dependancies to add:
    [quarkus.io] (https://code.quarkus.io/)

3. To start your quarkus appilcation run:
    nvm quarkus:dev

# Angular
1. To create your angular application run:
    ng new my-app

2. To run your application use:
    ng serve --open
    
* For more angualar detials go here:
    [angular.io] (https://angular.io/guide/setup-local)